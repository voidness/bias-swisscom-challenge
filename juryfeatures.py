import pandas as pd
from cbextractor import clean_company_name, get_crunchbase, get_full_crunchbase
from collections import Counter

schools = ['epfl', 'eth', 'zurich', 'mit', 'hsg', 'hec']


def read_jury_tags(jurors):
    # print(jurors)

    unique_tags = []
    tags = []
    for i in range(100):
        words = jurors.loc[i]['Words']
        if str(words) != 'nan':
            words = words.split(',')
            unique_tags += words
            words = [w.strip().lower() for w in words]
        else:
            words = []
        tags.append(words)
    return unique_tags, tags


def get_jury_schools(tags):
    # separate tags from schools
    jurorschools = []
    for j in tags:
        jschool = ''
        for t in j:
            if t in schools:
                jschool = t
        jurorschools.append(jschool)
    return jurorschools


def get_meaningful_tags(tags, unique_tags):
    # keep only if a tag appears more than once (very messy juror data)
    unique_tags = [t.strip().lower() for t in unique_tags]
    counted_tags = Counter(unique_tags)
    filtered_tags = [t for t in counted_tags if counted_tags[t] > 1]
    # print(filtered_tags)
    # print(tags)

    jurortags = []
    for j in tags:
        jtags = []
        for t in j:
            if t not in schools:
                if t in filtered_tags:
                    jtags.append(t)
        jurortags.append(jtags)
    return jurortags


if __name__ == "__main__":
    full = pd.read_csv(open('merged_data_final.csv'), index_col=0)
    jurors = pd.read_csv(open('data/jurydata_2018.csv'))

    unique_tags, tags = read_jury_tags(jurors)
    jurorschools = get_jury_schools(tags)
    jurortags = get_meaningful_tags(tags, unique_tags)

    schoolcount = Counter(jurorschools)
    schoolcount.pop('', None)

    # flatten last and count tag occurrences
    tagcount = Counter([item for sublist in jurortags for item in sublist])
    print(tagcount)

    jurortagmatch = []  # number of juror-tags that match
    jurorschoolmatch = []  # number of jurors from this university
    for i in range(full.shape[0]):
        c = 0
        desc = full.loc[i]['description']
        if str(desc) != 'nan':
            for t in tagcount:
                if t in desc:
                    c += tagcount[t]
        jurortagmatch.append(c)
        # school feature missing from company data list
    full['jurortagcount'] = pd.Series(jurortagmatch).values
    full.to_csv('merged_data_final.csv')

    # skip for now: extract categories from crunchbase on the juror's companies
    # for i in range(100):
    #     company = clean_company_name(jurors.loc[i]['Company'])
    #     print(company)
    #     cbinfo = get_crunchbase(company)
    #     print(cbinfo)
