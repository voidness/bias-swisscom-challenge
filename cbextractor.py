import cbconfig
import json
import re
import pickle
import requests


load_companies_from_file = True
load_crunchbase_from_file = True
load_full_crunchbase_from_file = True


def clean_company_name(name):
    company = name.lower().replace(' ag', ' ').replace(' sarl', ' ').replace(' sagl', ' ')
    company = company.replace(' sa', ' ').replace(' srl', '').replace(' gmbh', ' ').strip()
    company = re.sub(r'\([^)]*\)', '', company)
    return company


# read companies from file
def load_companies(companyfile):
    companies = []  # should be 668
    with open(companyfile, encoding='latin-1') as f:
        for line in f:
            line = line.split('\t')
            if len(line) > 4:
                company = clean_company_name(line[3])
                # print(company)
                companies.append(company)
            # else:
                # some weird cases where there are line breaks in the file...
                # print(line)

    return sorted(companies)


# query crunchbase for company names
def get_crunchbase(company):
    company = company.replace('&', '%26')  # escape & for request URL
    cbquery = 'https://api.crunchbase.com/v3.1/organizations?query=' + company + '&user_key=' + cbconfig.api_key
    try:
        data = requests.get(cbquery).json()['data']
        if data['paging']['total_items'] > 1:
            # print('pages', data['paging']['number_of_pages'])
            for i in data['items']:
                if i['properties']['country_code'] == 'Switzerland':
                    return i
        else:
            return data['items']

    except Exception as e:
        print(company, e)
        return None


# get the full crunchbase page for a company
def get_full_crunchbase(companyurl):
    cbquery = companyurl + '?user_key=' + cbconfig.api_key
    try:
        return requests.get(cbquery).json()
    except Exception as e:
        print(companyurl, e)
        return None


# get all the categories for a company
def get_categories(companydata):
    categories = []
    for c in companydata['data']['relationships']['categories']['items']:
        catpath = c['properties']['category_groups']
        if catpath is not None:
            categories.append(c['properties']['category_groups'] + [c['properties']['name']])
        else:
            categories.append([c['properties']['name']])
    return categories


if __name__ == "__main__":
    if load_companies_from_file:
        companies = pickle.load(open('data/companies.p', 'rb'))
    else:
        companies = load_companies('data/startup.ch 2013 - 2018.tsv')
        pickle.dump(companies, open('data/companies.p', 'wb'))

    if load_crunchbase_from_file:
        company_info = pickle.load(open('data/companies_crunchbase.p', 'rb'))
    else:
        company_info = dict()
        for company in companies:
            data = get_crunchbase(company)
            if data is not None and data != []:
                company_info[company] = data
        print('companies with info', len(company_info))
        pickle.dump(company_info, open('data/companies_crunchbase.p', 'wb'))

    if load_full_crunchbase_from_file:
        full_info = pickle.load(open('data/full_companies_crunchbase.p', 'rb'))
    else:
        full_info = dict()
        for company in company_info:
            if isinstance(company_info[company], list):
                company_info[company] = company_info[company][0]
            full_data = get_full_crunchbase(company_info[company]['properties']['api_url'])
            full_info[company] = full_data
        pickle.dump(full_info, open('data/full_companies_crunchbase.p', 'wb'))


    full_info = pickle.load(open('data/full_companies_crunchbase.p', 'rb'))
    for company in full_info:
        print(get_categories(full_info[company]))

