import pandas as pd
import pickle


def make_a_dict(datalist):
    adict = dict()
    for x in datalist:
        adict[x[0].lower()] = x[1:]
    return adict


full = pd.read_csv(open('extended_startup_data.csv'), index_col=0)
cb = pickle.load(open('data/company_features_crunchbase.p', 'rb'))
twitter = pd.read_csv(open('twitter_followers.csv')).values.tolist()

twitter = make_a_dict(twitter)

# columns to add
twitterfollowers = []
fundingusd = []
femaleratio = []
numberinvestors = []
fundingrounds = []
categories = []

if __name__ == "__main__":
    for i in range(1363):
        company = full.loc[i]['cleanname'].lower()
        try:
            followers = twitter[company][0]
        except Exception:
            followers = 0
        twitterfollowers.append(followers)

        try:
            funding = cb[company][0]
            female = cb[company][1]
            investors = cb[company][2]
            frounds = cb[company][3]
            cats = cb[company][4]
        except Exception:
            funding = female = investors = frounds = 0
            cats = []
        fundingusd.append(funding)
        femaleratio.append(female)
        numberinvestors.append(investors)
        fundingrounds.append(frounds)
        categories.append(cats)

    full['twitterfollowers'] = pd.Series(twitterfollowers).values
    full['fundingusd'] = pd.Series(fundingusd).values
    full['femaleratio'] = pd.Series(femaleratio).values
    full['numberinvestors'] = pd.Series(numberinvestors).values
    full['fundingrounds'] = pd.Series(fundingrounds).values
    full['categories'] = pd.Series(categories).values

    full.to_csv('merged_data_final.csv')
