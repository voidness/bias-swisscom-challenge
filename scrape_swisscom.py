from bs4 import BeautifulSoup
import requests
import pandas as pd

def get_year_soup(year):
	#page id for each year of swiss top 100
	pages = {
		2011: 129673,
		2012: 130940,
		2013: 132463,
		2014: 133468,
		2015: 134256,
		2016: 135186,
		2017: 129572
	}
	url = "https://www.startup.ch/index.cfm?page=%s&cfid=623181729&cftoken=47435454"%pages[year]

	return get_url_soup(url)
	
def get_url_soup(url):
	data = requests.get(url).text
	soup = BeautifulSoup(data, "html.parser")

	return soup

def get_sublink_data(url):
	alltags = []
	awards = []

	try:
		soup = get_url_soup(url)
		ul = soup.find_all("ul", class_="categorys cf")[0]
		tags = ul.find_all('li')
		alltags = [aw.text.strip('\n') for aw in tags]
	except Exception as e:
		pass
	
	try:
		awards_list = soup.find_all("ul", class_="arrow-list")[0]
		awards_list = awards_list.find_all('li')
		awards = [aw.text.strip('\n') for aw in awards_list]
	except Exception as e:
		pass

	description = soup.find_all("div", class_="column-left")[1]
	description = description.find_all("p")[0].text

	return alltags, awards, description


def make_csv():
	linkbase = "https://www.startup.ch/"

	df = pd.DataFrame(columns={'comp_year', 'rank', 'name', 'description', 'incorporated', 'place', 'age', 'tags', 'awards', 'awards_count'})
	index = 0
	for year in range(2011, 2018):
		print(year, flush=True)
		soup = get_year_soup(year)

		tables = soup.find_all("div", class_="list")
		table = tables[0].find_all("table")[0]

		for row in table.find_all('tr'):
			col = row.find_all('td')
			datarow = {'incorporated': 0, 'place': 0}
			datarow['comp_year'] = year
			for entry, key in zip(col, ['rank', 'name', 'description']):
				text = entry.text
				if key == 'name':
					if year == 2017:
						name, tmp = text.split('\n')[2:4]
					else:
						name, tmp = text.split('\n')[1:3]
					datarow[key] = name
					print(name, flush=True)

					try:
						place, incorporated = tmp.split(',')
						datarow['place'] = place
						datarow['incorporated'] = incorporated
					except Exception as e:
						pass
				else:
					text = text.replace('\n','')
					datarow[key] = text
			datarow['age'] = year - int(incorporated)
						
			# find link from column
			tags, awards, description = get_sublink_data(linkbase + col[1].a.get('href'))
			datarow['tags'] = tags
			datarow['awards'] = awards
			datarow['awards_count'] = len(awards)
			if len(description) > len(datarow['description']):
				datarow['description'] = description

			df.loc[index] = datarow
			
			index += 1
	df.to_csv('startup_data.csv')

def read_csv():
	df = pd.read_csv('all_startup_data.csv')
	names = df['name']
	acquireds = []
	extensions = []
	newnames = []
	vowels = []

	#letters in vowel as a list
	the_vowel = ["a","e","i","o","u"]

	for name in names:
		try:
			if 'acquired' in name:
				acquireds.append(1)
			else:
				acquireds.append(0)
		except Exception as e:
			acquireds.append(0)
		
		nameparts = name
		if name[0].lower() in the_vowel:
			vowels.append(1)
		else:
			vowels.append(0)
		if '(' in name:
			nameparts = name.split('(')[0][:-1]
		extension = nameparts.split(' ')
		if extension[-1] == '':
			extension = extension[:-1]

		if len(extension) > 1:
			extensions.append(extension[-1])
			newnames.append(' '.join(extension[:-1]))

		else:
			extensions.append('')
			newnames.append(extension[0])

	df['cleanname'] = newnames
	df['acquired'] = acquireds
	df['extension'] = extensions
	df['vowels'] = vowels
	df = df.drop(df.columns[0], axis=1)
	df.to_csv('extended_startup_data.csv', index_col = False)

def extend_data():
	df = pd.read_csv('extended_startup_data.csv', index_col = 0)
	columns = df.columns
	print(columns)
	df_swisscom = pd.read_csv('startup.ch2013-2018.csv', delimiter=';')
	df_swisscom = df_swisscom.rename(index=str, columns={"Company": "name", "City": "place", "Technology": "tags", "Incorporation":"incorporated", "1Liner" : "description"})
	df_subset = df_swisscom[['name', 'place', 'tags', 'description']]
	df_subset['comp_year'] = 2018
	df = df.append(df_subset, ignore_index=True)

	df.to_csv('all_startup_data.csv', index_col = False)

def scrape_experts():
	soup = get_url_soup("https://www.startup.ch/index.cfm?page=129355&cfid=623279271&cftoken=92926730")

	tables = soup.find_all("div", class_="list")
	table = tables[0].find_all("table")[0]
	print()

	for row in table.find_all('tr'):
		for col in row:
			print(col)
			# alltags = [aw.text.strip('\n') for aw in col]
			# print(alltags)

if __name__ == "__main__":
	# make_csv()

	read_csv()

	# extend_data()
	
	# scrape_experts()
