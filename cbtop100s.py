import pickle
import pandas

from cbextractor import get_crunchbase, get_full_crunchbase

YEAR = 2017


def get_top100_companies(year, path):
    top100s = pandas.read_csv(open(path), index_col=0)
    top100s = top100s[top100s['comp_year'] == year]  # filter year
    return list(top100s['cleanname'])


if __name__ == "__main__":
    companies = get_top100_companies(YEAR, 'all_startup_data.csv')
    company_info = dict()
    for company in companies:
        data = get_crunchbase(company)
        if data is not None and data != []:
            company_info[company] = data
    print('companies with info', len(company_info))
    pickle.dump(company_info, open('data/companies_crunchbase' + str(YEAR) + '.p', 'wb'))

    full_info = dict()
    for company in company_info:
        if isinstance(company_info[company], list):
            company_info[company] = company_info[company][0]
        full_data = get_full_crunchbase(company_info[company]['properties']['api_url'])
        full_info[company] = full_data
    pickle.dump(full_info, open('data/full_companies_crunchbase' + str(YEAR) + '.p', 'wb'))
