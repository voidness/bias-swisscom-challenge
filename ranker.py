import pandas as pd
from sklearn.preprocessing import StandardScaler
from libs.ranking import RankSVM, transform_pairwise
import numpy as np
from sklearn import svm, linear_model, model_selection
import pickle

if __name__ == '__main__':
    df = pd.read_csv('final_features.csv', index_col=0)

    cols_to_drop = ['rank', 'comp_year', 'categories']

    df_train = df[df.comp_year < 2017]
    df_val   = df[df.comp_year == 2017]
    df_test  = df[df.comp_year == 2018]

    X_train = df_train.drop(cols_to_drop, axis=1).values
    y_train = df_train['rank'].values

    X_val = df_val.drop(cols_to_drop, axis=1).values
    y_val = df_val['rank'].values

    X_test = df_test.drop(cols_to_drop, axis=1).values


    scaler = StandardScaler()
    scaler.fit(X_train)

    X_train = scaler.transform(X_train)
    X_val = scaler.transform(X_val)
    X_test = scaler.transform(X_test)


    # from example
    # n_samples = df.shape[0]
    # n_features = df.shape[1]
    # true_coef = np.random.randn(n_features)
    # X = np.random.randn(n_samples, n_features)
    # noise = np.random.randn(n_samples) / np.linalg.norm(true_coef)
    # y = np.dot(X, true_coef)
    # y = np.arctan(y) # add non-linearities
    # y += .1 * noise  # add noise
    # Y = np.c_[y, np.mod(np.arange(n_samples), 5)]  # add query fake id
    # cv = model_selection.KFold(n_splits=5)
    # train, test = iter(cv).next()

    # print the performance of ranking
    rank_svm = RankSVM().fit(X_train, y_train)
    print('Performance of ranking ', rank_svm.score(X_val, y_val))
    y_pred = rank_svm.decision_function(X_test)

    df = pd.read_csv('merged_data_final.csv')
    df = df[df.comp_year == 2018]
    cranks = zip(list(df['cleanname']), y_pred)
    pickle.dump(cranks, open('data/rankedcandidates.p', 'wb'))

    """# and that of linear regression
    ridge = linear_model.RidgeCV(fit_intercept=True)
    ridge.fit(X_train, y_train)
    X_test_trans, y_test_trans = transform_pairwise(X_val, y_val)
    score = np.mean(np.sign(np.dot(X_test_trans, ridge.coef_)) == y_test_trans)
    print('Performance of linear regression ', score)
    """
