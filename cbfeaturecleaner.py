import pickle
import pandas as pd
from cbextractor import get_categories

to_pandas = False

def get_femaleratio(data):
    men = 0.0
    women = 0.0
    for f in data['relationships']['founders']['items']:
        if f['properties']['gender'] == 'Male':
            men += 1
        elif f['properties']['gender'] == 'Female':
            women += 1
    return (women / men) if men != 0 else 0.0


def merge_data():
    company_candidates = pickle.load(open('data/full_companies_crunchbase.p', 'rb'))
    company_2011 = pickle.load(open('data/full_companies_crunchbase2011.p', 'rb'))
    company_2012 = pickle.load(open('data/full_companies_crunchbase2012.p', 'rb'))
    company_2013 = pickle.load(open('data/full_companies_crunchbase2013.p', 'rb'))
    company_2014 = pickle.load(open('data/full_companies_crunchbase2014.p', 'rb'))
    company_2015 = pickle.load(open('data/full_companies_crunchbase2015.p', 'rb'))
    company_2016 = pickle.load(open('data/full_companies_crunchbase2016.p', 'rb'))
    company_2017 = pickle.load(open('data/full_companies_crunchbase2017.p', 'rb'))

    # merge the dicts
    company_candidates.update(company_2011)
    company_candidates.update(company_2012)
    company_candidates.update(company_2013)
    company_candidates.update(company_2014)
    company_candidates.update(company_2015)
    company_candidates.update(company_2016)
    company_candidates.update(company_2017)
    return company_candidates


# put all companies in one csv, no duplicates
company_features = pd.DataFrame(columns=['company', 'fundingusd', 'femaleratio', 'numberinvestors', 'fundingrounds', 'categories'])
# company_features.set_index('company')
company_candidates = merge_data()

if to_pandas:
    for company in company_candidates:
        data = company_candidates[company]['data']
        company_features = company_features.append({
            'company': company,
            'fundingusd': data['properties']['total_funding_usd'],
            'femaleratio': get_femaleratio(data),
            'numberinvestors': data['relationships']['investors']['paging']['total_items'],
            'fundingrounds': data['relationships']['funding_rounds']['paging']['total_items'],
            'categories': get_categories(company_candidates[company])
            }, ignore_index=True)

    company_features.to_csv('companies_crunchbase_features.csv')

else:
    company_features = dict()
    for company in company_candidates:
        data = company_candidates[company]['data']
        company_features[company.lower()] = [
            data['properties']['total_funding_usd'],
            get_femaleratio(data),
            data['relationships']['investors']['paging']['total_items'],
            data['relationships']['funding_rounds']['paging']['total_items'],
            get_categories(company_candidates[company])
            ]

    pickle.dump(company_features, open('data/company_features_crunchbase.p', 'wb'))
