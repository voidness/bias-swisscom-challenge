# hack'n'lead team K7 - BIAS - Swisscom challenge
 
## __`Teamvision: Predicting Startup Succes`__

## __`Mission: Predicting Juror decisions for the Top100 startup competition`__

## __`Action: Correlate Juror, startup data and 'buzz words'+trends to top100 ranking`__

Problem definition:
https://medium.com/womenplusplus/hackn-lead-the-challenges-5e0fbcff70db

(summarized) Apply objectivity on startup rating through machine learning, use the TOP100startups.ch competition as an example case.

### Plan:
- Come up with the best strategy

(who will benefit, what do they need, how do we fix their problems)
- Write amazing code
- Pitch wonderful things

### Execution:
- Data gathering

(scraping from startup.ch:company description, when incorporated, headquarters, support, keywords, age, # of awards won)
more enrichment from the current market, funding



### NLP
- give value to quantify importance of keywords in startup descriptions
(words like: medtech, zurich, application, platform)
- 30 matched in a broad set
- We wanted to see wher

### Crunchbase:
- we extracted the funding of startups (rounds, amount)
- gender balance in founding members
- social media accounts of companies

### startup.ch:
- age
- awards (nr)
- description (keywords)
- added wether vowel was in the first letter

### twitter.com
- number of followers

## 2018 predicted Top 10
### Based on RankSVM
- Albedo Holding
- Urban Games
- embotech
- Attinger Technik
- Integration Alpha
- Omniroll
- PlayfulVision
- CBFS
- Vidytape
- Arca24.com


## Future enhancements
- incorporate information on the juror's biases: what school they attended and what field they are in may impact their funding choices
- incorporate more features and more data, e.g. from further social media sources
- more finely tune the machine learning models

