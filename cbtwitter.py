import json
import oauth2
import cbconfig
from cbfeaturecleaner import merge_data
import pandas as pd


def oauth_req(url, http_method='GET', post_body='', http_headers=None):
    consumer = oauth2.Consumer(key=cbconfig.consumer_key, secret=cbconfig.consumer_secret)
    token = oauth2.Token(key=cbconfig.access_token, secret=cbconfig.access_secret)
    client = oauth2.Client(consumer, token)
    resp, content = client.request(url)
    return content


def get_follower_count(twitter_user):
    twitter_url = 'https://api.twitter.com/1.1/users/show.json?screen_name=' + twitter_user
    result = oauth_req(twitter_url)
    try:
        return json.loads(result)['followers_count']
    except Exception:
        return 0


if __name__ == "__main__":
    company_data = merge_data()
    twitter_followers = dict()
    for company in company_data:
        for w in company_data[company]['data']['relationships']['websites']['items']:
            if w['properties']['website_type'] == 'twitter':
                twitter_username = w['properties']['url'].replace('https://twitter.com/', '').replace('https://www.twitter.com/', '').replace('http://twitter.com/', '').replace('http://www.twitter.com/', '').replace('/', '').replace('?lang=en', '')
                followers = get_follower_count(twitter_username)
                twitter_followers[company] = followers

    pd.DataFrame.from_dict(twitter_followers, orient='index').to_csv('twitter_followers.csv')

